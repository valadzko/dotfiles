set nocompatible              " be iMproved, required
filetype off                  " required
"test line kj sdflsdfk f asdf as f
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'gmarik/Vundle.vim'
Plugin 'whatyouhide/vim-gotham' "awesome colorscheme
Plugin 'Valloric/YouCompleteMe' "autocomplete code
Plugin 'nathanaelkane/vim-indent-guides' "show tabs in code
call vundle#end()            " required
filetype plugin indent on    " required

" Display line numbers
set number
" Tab settings
set tabstop=4
" visual autocomplete for command menu
set wildmenu
" highlight matching []{}()
set showmatch
" highlight matches 
set hlsearch

"Mappings
:inoremap jk <esc>
:map <C-X> <esc>:x<CR>
:imap <C-X> <esc>:x<CR>
syntax enable
set background=dark
colorscheme gotham256 
